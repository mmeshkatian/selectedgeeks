<?php
namespace Mehdi\SelectedGeeks\Helpers;

class Router
{
    public function __construct(private $routes)
    {

    }
    public static function new($routes)
    {
        return new Router($routes);
    }

    public function handle()
    {
        $uri = trim($_SERVER['REQUEST_URI'],'/');
        if(!empty($this->routes[$uri])){
            $router = $this->routes[$uri];
            return $this->run($router);
        }

        return $this->notFound();

    }

    private function notFound()
    {
        return Helper::jsonResponse(['status' => 'not found'], 404);
    }

    private function run($router)
    {
        $class  = $router[0];
        $method = $router[1];

        $instance = new $class;
        return $instance->$method();
    }
}