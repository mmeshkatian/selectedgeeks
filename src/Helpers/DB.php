<?php
namespace Mehdi\SelectedGeeks\Helpers;

class DB
{
    protected static $_instance;

    public function __construct($config)
    {
        $db = new \MysqliDb (
            $config['host'],
            $config['username'],
            $config['password'],
            $config['db'],
            "3306"
        );

        self::$_instance = $db;
    }

    private static function instance()
    {
        return self::$_instance;
    }

    public static function __callStatic($name, $args)
    {
        return self::instance()->$name(...$args);
    }
}