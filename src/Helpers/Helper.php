<?php
namespace Mehdi\SelectedGeeks\Helpers;

class Helper
{
    public static function jsonResponse($data, $statusCode = 200)
    {
        http_response_code($statusCode);
        header('Content-Type: application/json; charset=utf-8');

        return json_encode(['data' => $data]);
    }

    public static function validationErr($field, $msg = null)
    {
        return self::jsonResponse([
            $field => $msg ?? 'The ' . $field. ' is invalid'
        ], 422);
    }
}