<?php
namespace Mehdi\SelectedGeeks\Controllers;

use Mehdi\SelectedGeeks\Helpers\Helper;
use Mehdi\SelectedGeeks\Importers\ImporterFactory;
use Mehdi\SelectedGeeks\Exceptions\ImportException;

class ImportController
{
    public function import()
    {
        $file = $_FILES['file'] ?? null;

        if(empty($file)){
            return Helper::validationErr('file');
        }

        try {
            $factory = ImporterFactory::make($file['type']);
            $factory->importFile($file['tmp_name']);

        }catch (ImportException $e){
            return Helper::validationErr('file', $e->getMessage());
        }catch (\Exception $e){
            // TODO: log the exception
            return Helper::validationErr('file', 'there was an critical error. please contact site admin');
        }

        return Helper::jsonResponse([
            'message' => 'import was successful'
        ]);
    }
}