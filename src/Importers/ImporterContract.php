<?php
namespace Mehdi\SelectedGeeks\Importers;

interface ImporterContract
{
    public function importFile($file);
}