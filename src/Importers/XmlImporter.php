<?php
namespace Mehdi\SelectedGeeks\Importers;

use Mehdi\SelectedGeeks\Helpers\DB;
use Mehdi\SelectedGeeks\Exceptions\ImportException;

class XmlImporter extends BaseImporter implements ImporterContract
{
    public function importFile($file)
    {
        $products = json_decode(json_encode(simplexml_load_file($file)), true)['Product'];

        if(!$products){
            throw new ImportException("selected xml file is invalid");
        }

        foreach ($products as $i => $product) {
            DB::startTransaction();
            try {
                $this->importProduct($product);
            }catch (\Exception $e){
                // TODO log the error
                DB::rollback();
                throw new ImportException("there was an error on line {$i} so nothing were imported");
            }

            DB::commit();
        }
    }

    // in case of XML there are some differences in the structure so we should handle it like this
    protected function importItems($productId, $items)
    {
        // fix the one child array problem on XML importer
        if(!empty($items['Item']['SKU'])){
            $items = [$items['Item']];
        }else{

            $items = $items['Item'];
        }

        foreach ($items as $item) {
            $this->importItem($productId, $item);
        }
    }
}