<?php

namespace Mehdi\SelectedGeeks\Importers;

use Mehdi\SelectedGeeks\Helpers\DB;
use Mehdi\SelectedGeeks\Exceptions\ImportException;

class BaseImporter
{
    protected function productExists($id)
    {
        return !empty(DB::where("id", $id)->getOne('products')['id'] ?? null);
    }

    protected function importProduct($data)
    {
        // TODO: validate the input data and show proper error

        if($this->productExists($data['Product_ID'])){
            return;
        }

        $productId = DB::insert('products', [
            'id'                =>  $data['Product_ID'],
            'nr'                =>  $data['NR'],
            'name'              =>  $data['Name'],
            'search_keywords'   =>  json_encode(explode(",", $data['Search_Keywords'])),
            'description'       =>  $data['Description'],
            'category_id'       => $this->findOrCreate('categories', ['id' => $data['Category_ID']], ['title' => $data['Category']]),
            'sub_category_id'   => $this->findOrCreate('sub_categories', ['id' => $data['SubCategory_ID']], ['title' => $data['SubCategory']]),
            'brand_id'          => $this->findOrCreate('brands', ['title' => $data['Brand']]),
        ]);

        $this->importItems($productId, $data['Items']);
    }

    protected function importItems($productId, $items)
    {
        foreach ($items as $item) {
            $this->importItem($productId, $item);
        }
    }

    protected function importItem($productId, $item)
    {
        $itemId = $this->findOrCreate(
            'product_items',
            ['product_id' => $productId, 'sku' => $item['SKU']],
            [
                'price'             => $item['Price'],
                'retail_price'      => $item['Retail_Price'],
                'thumbnail_url'     => $item['Thumbnail_URL'],
                'occassion'         => json_encode($item['Occassion']),
                'season'            => json_encode($item['Season']),
                'rating_avg'        => $item['Rating_Avg'],
                'rating_count'      => $item['Rating_Count'],
                'active'            => $item['Active'] ?? 0,
                'color_id'          => $this->findOrCreate('colors', ['title' => $item['Color'], 'family' => $item['Color_Family']]),
                'size_id'           => $this->findOrCreate('sizes', ['title' => $item['Size']], ['family' => $item['Size_Family']]),
            ]);
        foreach ($item['Warehouse'] as $warehouse => $wData){
            $warehouse = $this->findOrCreate('warehouses', ['title' => $warehouse]);
            $this->findOrCreate('warehouse_quantity',
                [
                    'warehouse_id' => $warehouse,
                    'product_item_id' => $itemId
                ],
                ['quantity' => $wData['Inventory_Count']]
            );
        }
    }

    protected function findOrCreate($table, $wheres = [], $data = [])
    {
        $id = DB::getInstance();

        foreach ($wheres as $field => $value) {
            $id->where($field, $value);
        }

        $id = $id->getOne($table)['id'] ?? null;

        if(!$id){
            $id = DB::insert($table, array_merge($wheres, $data));
        }

        return $id;
    }
}