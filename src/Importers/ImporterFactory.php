<?php
namespace Mehdi\SelectedGeeks\Importers;

use Mehdi\SelectedGeeks\Exceptions\ImportException;

class ImporterFactory
{
    public static function make($ext) : ImporterContract
    {
        return new (match ($ext){
            'application/json'  => JsonImporter::class,
            'application/xml'   => XmlImporter::class,
            default             => throw new ImportException("unsupported file")
        });

    }
}