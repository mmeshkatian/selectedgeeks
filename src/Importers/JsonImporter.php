<?php

namespace Mehdi\SelectedGeeks\Importers;

use Mehdi\SelectedGeeks\Helpers\DB;
use Mehdi\SelectedGeeks\Exceptions\ImportException;

class JsonImporter extends BaseImporter implements ImporterContract
{
    public function importFile($file)
    {
        $products = json_decode(file_get_contents($file), true);

        if(json_last_error() != JSON_ERROR_NONE){
            throw new ImportException("selected json file is invalid");
        }

        foreach ($products as $i => $product) {
            DB::startTransaction();

            try {
                $this->importProduct($product);
            }catch (\Exception $e){
                // TODO log the error
                DB::rollback();
                throw new ImportException("there was an error on line {$i} so nothing were imported");
            }

            DB::commit();
        }
    }


}