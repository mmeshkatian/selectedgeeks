<?php

use Mehdi\SelectedGeeks\Helpers\DB;
use Mehdi\SelectedGeeks\Helpers\Router;
use Mehdi\SelectedGeeks\Controllers\ImportController;

require __DIR__.'/vendor/autoload.php';

$db = new DB([
   'host'       => 'mysql',
   'username'   => 'root',
   'password'   => '123456',
   'db'         => 'selected'
]);

$router = Router::new([
   'upload' => [ImportController::class, 'import']
]);

echo $router->handle();