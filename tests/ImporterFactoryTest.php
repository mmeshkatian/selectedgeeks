<?php
use PHPUnit\Framework\TestCase;
use Mehdi\SelectedGeeks\Importers\JsonImporter;
use Mehdi\SelectedGeeks\Importers\ImporterFactory;
use Mehdi\SelectedGeeks\Exceptions\ImportException;

final class ImporterFactoryTest extends TestCase
{
    public function test_it_can_make_importer_from_ext_name()
    {
        $importer = ImporterFactory::make('application/json');

        $this->assertEquals(JsonImporter::class, get_class($importer));
    }

    public function test_it_will_throw_error_when_ext_is_unsupported()
    {
        $this->expectException(ImportException::class);

        $importer = ImporterFactory::make('application/word');
    }
}
